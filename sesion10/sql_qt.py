# QTableView
# QTreeView

# QStandardItemMode

# Ex 1: QTableView


import sys
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtSql import (QSqlDatabase, QSqlTableModel,
                           QSqlRelationalDelegate,
                           QSqlQueryModel,
                           QSqlRelationalTableModel, QSqlRelation)
from PySide6.QtCore import Qt, QSize

from datetime import datetime

from pathlib import Path 
import pandas as pd

from PySide6.QtWidgets import (
    QApplication,
    QLineEdit,
    QMainWindow,
    QTableView,
    QVBoxLayout,
    QWidget,
)
def connection():
    db  = QSqlDatabase("QSQLITE")
    db.setDatabaseName("chinook.sqlite")
    db.open()
    return db


db = connection()
import re


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        container = QWidget()
        layout = QVBoxLayout()
        self.search = QLineEdit()
        self.search.textChanged.connect(self.update_filter)
        
        layout.addWidget(self.search)

        self.table = QtWidgets.QTableView()
        layout.addWidget(self.table)

        self.db = connection()
        self.model = QSqlRelationalTableModel(db=db)
        self.table.setModel(self.model)
        self.model.setTable("Track")
        self.model.setRelation(2,
                               QSqlRelation("Album","AlbumId","Title"))
        self.model.setRelation(3, QSqlRelation("MediaType","MediaTypeId","Name"))
        self.model.setRelation(4, QSqlRelation("Genre","GenreId","Name"))

        columns_to_remove = ["GenreId", "Composer"]
        for n in columns_to_remove:
            idx = self.model.fieldIndex(n)
            self.model.removeColumns(idx, 1)

        columns = {
            "AlbumID": "Album (ID)",
            "MediaTypeId": "Media Type (ID)",
            "GenreId": "Genre (ID)",
        }
        for n, t in columns.items():
            idx = self.model.fieldIndex(n)
            self.model.setHeaderData(idx, Qt.Horizontal, t)
        idx = self.model.fieldIndex("Milliseconds")       
        self.model.setSort(idx, Qt.DescendingOrder)

        delegate = QSqlRelationalDelegate(self.table)
        self.table.setItemDelegate(delegate)

        
        self.model.select()

        container.setLayout(layout)

        self.setCentralWidget(container)
        self.setMinimumSize(QSize(1024, 600))


    def update_filter(self, s):
        s = re.sub("[\W_]+", "", s)
        filter_str = 'Name LIKE "%{}%"'.format(s) 
        self.model.setFilter(filter_str)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()

