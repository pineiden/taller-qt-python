# QTableView
# QTreeView

# QStandardItemMode

# Ex 1: QTableView


import sys
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtSql import (QSqlDatabase, QSqlTableModel,
                           QSqlRelationalDelegate,
                           QSqlQueryModel, QSqlQuery,
                           QSqlRelationalTableModel, QSqlRelation)
from PySide6.QtCore import Qt, QSize

from datetime import datetime

from pathlib import Path 
import pandas as pd

from PySide6.QtWidgets import (
    QApplication,
    QLineEdit,
    QMainWindow,
    QTableView,
    QVBoxLayout,
    QWidget,
)
def connection():
    db  = QSqlDatabase("QSQLITE")
    db.setDatabaseName("chinook.sqlite")
    db.open()
    return db


db = connection()
import re


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.table = QTableView()

        self.model = QSqlQueryModel()
        self.table.setModel(self.model)

        query = QSqlQuery("SELECT Name, Composer FROM track ", db=db)
        self.model.setQuery(query)

        self.setCentralWidget(self.table)
        self.setMinimumSize(QSize(1024, 600))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()

