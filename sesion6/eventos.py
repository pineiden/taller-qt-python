import sys
from random import randint
from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QLineEdit,)


class MainWindow(QMainWindow):
    # ventanas = []
    def __init__(self):
        super().__init__()
        self.label = QLabel(
            f"Click en la ventana")
        self.setCentralWidget(self.label)
        self.setMouseTracking(True)

    def mouseMoveEvent(self, e):
        self.label.setText("Mouse de mueve")


    def mousePressEvent(self, e):
        if e.button() == Qt.LeftButton:
            pos = e.pos()
            gpos = e.globalPos()
            self.label.setText(f"Se presionó botón izquierdo del mouse, pos {pos}, global {gpos}")
        if e.button() == Qt.MiddleButton:
            self.label.setText("Se presionó botón medio del mouse")
        if e.button() == Qt.RightButton:
            self.label.setText("Se presionó botón derecho del mouse ")

    def mouseReleasEvent(self, e):
        if e.button() == Qt.LeftButton:
            self.label.setText("Se liberó botón izquierdo del mouse ")

        if e.button() == Qt.MiddleButton:
            self.label.setText("Se liberó botón medio del mouse")

        if e.button() == Qt.RightButton:
            self.label.setText("Se liberó botón derecho del mouse ")

    def mouseDoubleClickEvent(self, e):
        if e.button() == Qt.LeftButton:
            self.label.setText("Se hizo doble click botón izquierdo del mouse ")

        if e.button() == Qt.MiddleButton:
            self.label.setText("Se hizo doble click botón medio del mouse")

        if e.button() == Qt.RightButton:
            self.label.setText("Se hizo doble click botón derecho del mouse ")

if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()



