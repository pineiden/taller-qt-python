import sys
from random import randint

from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QLineEdit,)


class OtraVentana(QWidget):
    """
    Esto es una ventana hereda de QWidget.
    Aparecerá flotando sobre la ventana principal
    """
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        aleatorio  = randint(0, 100)
        self.label = QLabel(
            f"Otra ventama, numero {aleatorio}")
        layout.addWidget(self.label)
        self.setLayout(layout)


class MainWindow(QMainWindow):
    # ventanas = []
    def __init__(self):
        super().__init__()
        # ahora creamos la ventana al iniciar
        # pero la ocultamos
        self.w = OtraVentana()
        self.w.hide()
        self.button = QPushButton("Crear Ventana")
        self.button.clicked.connect(
            self.mostrar_otra_ventana)
        # evento ligado a la otra ventana.
        self.winput = QLineEdit()
        self.winput.textChanged.connect(
            self.w.label.setText)
        layout = QVBoxLayout()
        layout.addWidget(self.button)
        layout.addWidget(self.winput)
        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)


    def mostrar_otra_ventana(self, checked):
        # crear
        if self.w is None:
            self.w = OtraVentana()
            self.w.show()
        elif not self.w.isVisible():
            self.w.show()
        else:
            #self.w = None
            self.w.hide()
        # self.ventanas.append(w)
        # print(self.ventanas)
        # limpiar


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()



