import sys
from random import randint
from PySide6.QtCore import Qt
from PySide6.QtGui import QAction
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QLineEdit,
    QMenu)


class MainWindow(QMainWindow):
    # ventanas = []
    def __init__(self):
        super().__init__()
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)

    def on_context_menu(self, pos):
        context = QMenu(self)
        context.addAction(QAction("test 1", self))
        context.addAction(QAction("test 2", self))
        context.addAction(QAction("test 3", self))
        context.exec(self.mapToGlobal(pos))

class SegundaVentana(MainWindow):
    def __init__(self):
        super().__init__()

    def on_context_menu(self, pos):
        print("Se  presionó mouse")
        super().on_context_menu(pos)

if __name__ == "__main__":
    app = QApplication()
    window = SegundaVentana()
    window.show()
    app.exec()



