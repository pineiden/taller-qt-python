import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QCheckBox)

class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # etiqueta. 
        widget = QCheckBox("Esto es un checkbox")
        widget.setCheckState(Qt.Unchecked)
        widget.setTristate(True)

        widget.stateChanged.connect(self.show_estado)
        self.setCentralWidget(widget)

    def show_estado(self, s):
        print("Estado de checkbox",  s == Qt.Checked)
        print(s)

if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
