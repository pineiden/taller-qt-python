import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QSlider,
    QLabel,
    QVBoxLayout,
    QWidget)


class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # etiqueta. 
        widget = QSlider()
        widget.setMinimum(-5.0)
        widget.setMaximum(5.0)
        widget.setSingleStep(.5)
        # conectamos los eventos
        self.widget = widget
        self.slider_events()

        # label widget
        self.label = QLabel()
        font = self.label.font()
        font.setPointSize(30)
        self.label.setFont(font)

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.widget)
 
        container = QWidget()
        container.setLayout(layout)

        self.setCentralWidget(container)

    def slider_events(self):
        self.widget.valueChanged.connect(self.value_changed)
        self.widget.sliderMoved.connect(self.slider_position)
        self.widget.sliderPressed.connect(self.slider_pressed)
        self.widget.sliderReleased.connect(self.slider_released)

    def value_changed(self, i):
        print("Value", i)
        self.label.setText(str(i))

    def slider_position(self, p):
        print("position", p)

    def slider_pressed(self):
        print("slider presionado")

    def slider_released(self):
        print("slider liberado")

    def text_edited(self, s):
        print("Text", s)
        print(s)
        self.label.setText(s.upper())


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
