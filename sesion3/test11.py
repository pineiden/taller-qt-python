import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QDoubleSpinBox,
    QLabel,
    QVBoxLayout,
    QWidget)


class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # etiqueta. 
        widget = QDoubleSpinBox()
        widget.setMinimum(-5.0)
        widget.setMaximum(35.0)
        widget.setPrefix("Temp ")
        widget.setSuffix("[°C]")
        widget.setSingleStep(.5)
        # conectamos los eventos
        self.widget = widget
        self.spin_events()

        # label widget
        self.label = QLabel()
        font = self.label.font()
        font.setPointSize(30)
        self.label.setFont(font)

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.widget)
 
        container = QWidget()
        container.setLayout(layout)

        self.setCentralWidget(container)

    def spin_events(self):
        self.widget.valueChanged.connect(self.value_changed)
        self.widget.textChanged.connect(self.value_changed_str)

    def value_changed(self, i):
        print("Value", i)

    def value_changed_str(self, s):
        print(s)
        self.label.setText(s)

    def text_edited(self, s):
        print("Text", s)
        print(s)
        self.label.setText(s.upper())


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
