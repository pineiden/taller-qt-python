import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QComboBox,
    QLabel,
    QVBoxLayout,
    QWidget)


class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # etiqueta. 
        widget = QComboBox()
        widget.addItems(["Primero", "Segundo", "Tercero"])
        widget.setEditable(True)
        widget.setInsertPolicy(QComboBox.InsertAlphabetically)
        # conectamos los eventos
        widget.currentIndexChanged.connect(self.index_changed)
        widget.currentTextChanged.connect(self.text_changed)
        # label widget
        self.label = QLabel()
        font = self.label.font()
        font.setPointSize(30)
        self.label.setFont(font)

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(widget)
 
        container = QWidget()
        container.setLayout(layout)

        self.setCentralWidget(container)

    def index_changed(self, i):
        print("Index", i)

    def text_changed(self, t):
        print("Text", t)
        self.label.setText(t.upper())

if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
