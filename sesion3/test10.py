import sys

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QLineEdit,
    QLabel,
    QVBoxLayout,
    QWidget)


class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # etiqueta. 
        widget = QLineEdit()
        widget.setMaxLength(24)
        widget.setPlaceholderText("Escribe acá un texto")
        # conectamos los eventos
        self.widget = widget
        self.line_events()

        # label widget
        self.label = QLabel()
        font = self.label.font()
        font.setPointSize(30)
        self.label.setFont(font)

        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.widget)
 
        container = QWidget()
        container.setLayout(layout)

        self.setCentralWidget(container)

    def line_events(self):
        self.widget.returnPressed.connect(self.return_pressed)
        self.widget.selectionChanged.connect(self.selection_changed)
        self.widget.textChanged.connect(self.text_changed)
        self.widget.textEdited.connect(self.text_edited)

    def item_changed(self, i):
        print("Item", i.text)

    def return_pressed(self):
        print("Enter presionado")
        self.setWindowTitle(self.widget.text())

    def selection_changed(self):
        print("Cambio en la selección")
        print(self.widget.selectedText())

    def text_changed(self, t):
        print("Text", t)
        self.label.setText(t.upper())

    def text_edited(self, s):
        print("Text", s)
        print(s)
        self.label.setText(s.upper())


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
