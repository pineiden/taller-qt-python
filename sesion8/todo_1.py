from PySide6 import QtCore, QtGui
from PySide6.QtCore import Qt

tick = QtGui.QImage('tick.png')


class TodoModel(QtCore.QAbstractListModel):
    def __init__(self, todos=[]):
        super().__init__()
        self.todos = todos

    def add(self, action):
        self.todos.append((False, action))

    def delete(self, index):
        del self.todos[index.row()]

    def complete(self, index):
        row = index.row()
        status, text = self.todos[row]
        self.todos[row] = (True, text)
        self.dataChanged.emit(index, index)

    def data(self, index, role):
        """
        index es un objecto QModelIndex, no int
        role si el dato se ve o no
        """
        if role == Qt.DisplayRole:
            """
            status: bool 
            text: str 
            """
            status, text = self.todos[index.row()]
            return text

        if role == Qt.DecorationRole:
            status, text =  self.todos[index.row()]
            if status:
                return tick

    def rowCount(self, index):
        """
        index: QModelIndex
        """
        return len(self.todos)


