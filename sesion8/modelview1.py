import sys 
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtCore import Qt
from MainWindow import Ui_MainWindow
from todo_1 import TodoModel
import json
from pathlib import Path

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.model = TodoModel([(False, "Lavar los platos")])
        self.load()
        self.todoView.setModel(self.model)
        self.addButton.pressed.connect(self.add)
        self.deleteButton.pressed.connect(self.delete)
        self.completeButton.pressed.connect(self.complete)


    def add(self):
        """
        Añade item a nuestra lista Todo del modelo inicializado.
        tomado desde el widget QLineEdit y lo limpia
        """
        text = self.todoEdit.text()
        text = text.strip()
        if text:
            self.model.add(text)
            #refrescar la vista -> activa un trigger: gatillo
            self.model.layoutChanged.emit()
            # limpiar input 
            self.todoEdit.setText("")
        self.save()

    def delete(self):
        indexes = self.todoView.selectedIndexes()
        for index in indexes:
            self.model.delete(index)
            self.todoView.clearSelection()
        self.model.layoutChanged.emit()
        self.save()

    def complete(self):
        indexes = self.todoView.selectedIndexes()
        for index in indexes:
            self.model.complete(index)
            self.todoView.clearSelection()
        self.model.layoutChanged.emit()
        self.save()

    def load(self):
        data_json_path = Path("data.json")
        if not data_json_path.exists():
            with open(data_json_path, "w") as f:
                json.dump([], f)
        try:
            with open('data.json', 'r') as f:
                self.model.todos = json.load(f)
        except Exception as e:
            print("El archivo data.json no existe")
            raise e

    def save(self):
        with open('data.json', 'w') as f:
            data = json.dump(self.model.todos, f)





if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()
