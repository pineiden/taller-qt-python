import sys

from PySide6.QtCore import Qt 
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QHBoxLayout,
    QVBoxLayout,
    QLabel,
    QWidget)

from color_widget import Color


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle(
            "My app test VERTICAL layout")
        # crear 3 layouts principales .
        layout_1 = QHBoxLayout()
        layout_2 = QVBoxLayout()
        layout_3 = QVBoxLayout()
        # propiedades de espaciado
        layout_1.setContentsMargins(2, 2, 2, 2)
        layout_1.setSpacing(5)

        # 
        colores = {
            "l1": ("green", ),
            "l2": ("red", "blue", "purple"),
            "l3": ("red", "purple")
        }
        asignacion = lambda layout, color: layout.addWidget(
            Color(color))
        #list(map(asignacion, colores))      
        [asignacion(layout_2, color) for color in colores["l2"]]


        layout_1.addLayout(layout_2)

        [asignacion(layout_1, color) for color in colores["l1"]]
        
        [asignacion(layout_3, color) for color in colores["l3"]]

        layout_1.addLayout(layout_3)
        # for color in colores:
        #     layout.addWidget(Color(color))
        # crear un widget 
        widget = QWidget()
        widget.setLayout(layout_1)
        self.setCentralWidget(widget)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
