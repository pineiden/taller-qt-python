import sys

from PySide6.QtCore import Qt 
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QLabel,
    QWidget,
    QTabWidget,
    QPushButton)

from color_widget import Color


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle(
            "My app test VERTICAL layout")
        # crear el tab administrador .

        tabs = QTabWidget()
        tabs.setTabPosition(QTabWidget.West)
        tabs.setMovable(True)

        colores = ("red",
             "green",
             "blue",
             "yellow")
        asignacion = lambda tabs, color: tabs.addTab(
            Color(color), color)
        #list(map(asignacion, colores))      
        [asignacion(tabs, color) 
         for color in colores]
        self.setCentralWidget(tabs)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
