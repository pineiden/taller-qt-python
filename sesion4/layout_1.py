import sys

from PySide6.QtCore import Qt 
from PySide6.QtWidgets import (QApplication, QMainWindow)

from color_widget import Color


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("My app test layout")
        colorcito = "#7dff00"
        widget = Color(colorcito)

        self.setCentralWidget(widget)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
