import sys

from PySide6.QtCore import Qt 
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QStackedLayout,
    QVBoxLayout,
    QLabel,
    QWidget, 
    QPushButton)

from color_widget import Color


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle(
            "My app test VERTICAL layout")
        # crear 3 layouts principales .
        layout = QStackedLayout()
        # propiedades de espaciado

        # 
        colores = ("red",
             "green",
             "blue",
             "yellow")
        asignacion = lambda layout, color: layout.addWidget(
            Color(color))
        #list(map(asignacion, colores))      
        [asignacion(layout, color) 
         for color in colores]
        self.n = 3
        layout.setCurrentIndex(self.n)
        
        self.layout = layout
        button = QPushButton("Cambiar Widget")
        button.clicked.connect(self.cambiar_widget)
        # for color in colores:
        #     layout.addWidget(Color(color))
        # crear un widget 
        widget_colores = QWidget()
        widget_colores.setLayout(layout)
        layout_vertical = QVBoxLayout()
        layout_vertical.addWidget(widget_colores)
        layout_vertical.addWidget(button)
        widget = QWidget()
        widget.setLayout(layout_vertical)
        self.setCentralWidget(widget)

    def cambiar_widget(self):
        self.n -= 1
        self.layout.setCurrentIndex(self.n)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
