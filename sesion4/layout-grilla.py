import sys

from PySide6.QtCore import Qt 
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QGridLayout,
    QLabel,
    QWidget)

from color_widget import Color


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle(
            "My app test VERTICAL layout")
        # crear 3 layouts principales .
        layout = QGridLayout()
        # propiedades de espaciado

        # 
        colores = {
            (0, 0): "red",
            (1, 0): "green",
            (1, 1): "blue",
            (2, 1): "purple"
        }
        asignacion = lambda layout, color, pos: layout.addWidget(
            Color(color), *pos)
        #list(map(asignacion, colores))      
        [asignacion(layout, color, pos) 
         for pos, color in colores.items()]

        # for color in colores:
        #     layout.addWidget(Color(color))
        # crear un widget 
        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
