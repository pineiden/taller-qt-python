import sys

from PySide6.QtCore import Qt 
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QVBoxLayout,
    QWidget)

from color_widget import Color


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle(
            "My app test VERTICAL layout")
        # crear un layout vertical.
        layout = QVBoxLayout()
        colores = ["red", "blue", "black", "#79ff00"]
        asignacion = lambda color: layout.addWidget(
            Color(color))
        #list(map(asignacion, colores))      
        [asignacion(color) for color in colores]
        # for color in colores:
        #     layout.addWidget(Color(color))
        # crear un widget 
        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
