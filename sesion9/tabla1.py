# QTableView
# QTreeView

# QStandardItemMode

# Ex 1: QTableView


import sys
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6.QtCore import Qt

from datetime import datetime

from pathlib import Path 
import pandas as pd

COLORS = [
    '#053061', 
    '#2166ac', 
    '#4393c3', 
    '#92c5de', 
    '#d1e5f0',
    '#f7f7f7', 
    '#fddbc7', 
    '#f4a582', 
    '#d6604d', 
    '#b2182b', 
    '#67001f']


PATH = Path(__file__).absolute().parent.parent

class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data):
        super().__init__()
        self._data = data

    def data(self, index, role):
        """
        QIndex -> index.row(), index.column()
        """
        if role == Qt.DisplayRole:
            value = self._data.iloc[index.row()][index.column()]
            
            if isinstance(value, datetime):
                return value.strftime("%Y-%m-%d")

            if isinstance(value, float):
                return f"{value:.2f}"

            if isinstance(value, str):
                return f'"{value}"'

            return value

        # if role == Qt.BackgroundRole:
        #     value = self._data[index.row()][index.column()]
        #     if isinstance(value, int) or isinstance(value, float):
        #         value = int(value) 
        #         value = max(-5 , value)
        #         value = min(5, value)
        #         value = value + 5 # rango de 0 -> 10
        #         return QtGui.QColor(COLORS[value])

        if role == Qt.TextAlignmentRole:
            value = self._data.iloc[index.row()][index.column()]
            if isinstance(value, int) or isinstance(value, float):
                return Qt.AlignRight 

        if role == Qt.ForegroundRole:
            value = self._data.iloc[index.row()][index.column()]
            if (isinstance(value, int) or isinstance(value, float)) and value< 0:
                return QtGui.QColor("red")

        if role == Qt.DecorationRole:
            value = self._data.iloc[index.row()][index.column()]
            if isinstance(value, datetime):
                icon_path = PATH / "icons/icons/calendar.png"
                return QtGui.QIcon(str(icon_path))
            if isinstance(value,bool):
                if value:
                    tick_path = PATH / "icons/icons/tick.png"
                    return QtGui.QIcon(str(tick_path))
                    
                return QtGui.QIcon("../icons/icons/cross.png")

            if isinstance(value, int) or isinstance(value, float):
                value = int(value) 
                value = max(-5 , value)
                value = min(5, value)
                value = value + 5 # rango de 0 -> 10
                return QtGui.QColor(COLORS[value])
                

    def rowCount(self, index):
        return self._data.shape[0]

    def columnCount(self, index):
        return self._data.shape[1]


    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return str(self._data.columns[section])
            if orientation == Qt.Vertical:
                return str(self._data.index[section])

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.table = QtWidgets.QTableView()

        dataset = [
            [False, -9.34343, -1],
            [1, -1, "hola"],
            [3, True, -5],
            [3, 3, datetime(2021,12,11)],
            [datetime(2019,5,4), 8, True],
        ]
        data = pd.DataFrame(
            dataset, 
            columns=["A", "B", "C"], 
            index=[str(i) for i in range(len(dataset))])

        self.model = TableModel(data)

        self.table.setModel(self.model)


        self.setCentralWidget(self.table)
        self.setGeometry(600, 100, 400, 200)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()

