import sys

from PySide6.QtCore import QSize, Qt
from PySide6.QtWidgets import (
    QApplication, 
    QMainWindow,
    QPushButton)
# QWidget :: objeto general para widget 
# QPushButton :: objeto particular, hereda de QWidget


class MainWindow(QMainWindow):
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # crea un boton con un texto
        button = QPushButton("Presioname :)")
        # pone un botón en ventana
        self.setCentralWidget(button)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
