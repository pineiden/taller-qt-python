# es de python libreria estandar: std
import sys
# QT para Python
# qt5 -> Pyside2
from PySide6.QtWidgets import QApplication, QWidget

# sys.argv 
# python test.py hola
if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = QWidget()
    window.show()
    app.exec()
