from PySide6.QtWidgets import QApplication, QMainWindow
import sys
# QWidget :: objeto general para widget 
# QPushButton :: objeto particular, hereda de QWidget


if __name__ == "__main__":
    app = QApplication([])
    window = QMainWindow()
    window.show()
    app.exec()
