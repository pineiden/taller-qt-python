import sys

from pathlib import Path

from PySide6.QtCore import Qt, QSize
from PySide6.QtGui import QAction, QIcon
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QToolBar,
    QStatusBar,
    QMainWindow)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Mi app con Toolbar")
        
        label = QLabel("Holanda que talca!")

        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)
        
        toolbar = QToolBar("Barra de botones")
        toolbar.setIconSize(QSize(52, 52))

        self.addToolBar(toolbar)
        button_action, dark_mode = self.setQAction(
            toolbar)
        toolbar.addAction(button_action)
        toolbar.addSeparator()
        toolbar.addAction(dark_mode)
        self.setStatusBar(QStatusBar(self))

    def OnClickBotonAccion(self, s):
        print("click", s)


    def setQAction(self, toolbar):
        filepath = Path(__file__).absolute().parent
        # print(filepath)
        icon_path = filepath.parent / "icons/icons/bug.png"
        dark_mode_icon = filepath.parent / "icons/propios/modo_nocturno.svg"
        # print("Existe icono", 
        #       icon_path.exists(), 
        #       type(icon_path))
        # toolbar.setToolButtonStyle(
        #     Qt.ToolButtonIconOnly)
        button_action = QAction(
            QIcon(str(icon_path)), "Acción", self)
        button_action.setToolTip(
            "Este es un botón de acción")
        button_action.setStatusTip(
            "Acción")

        button_action.triggered.connect(
            self.OnClickBotonAccion)

        button_action.setCheckable(True)

        dark_mode = QAction(
            QIcon(str(dark_mode_icon)), "Modo Oscuro", self)
        dark_mode.setToolTip(
            "Cambiar a modo oscuro")
        dark_mode.setStatusTip(
            "Modo Oscuro")

        dark_mode.triggered.connect(
            self.OnClickBotonAccion)

        dark_mode.setCheckable(True)


        return button_action, dark_mode


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
