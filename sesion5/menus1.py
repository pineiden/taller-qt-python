import sys

from pathlib import Path

from PySide6.QtCore import Qt, QSize
from PySide6.QtGui import QAction, QIcon
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QToolBar,
    QStatusBar,
    QMainWindow)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("App con menús")

        label = QLabel("App con Menú")
        label.setAlignment(Qt.AlignCenter)

        self.setCentralWidget(label)
        toolbar = QToolBar("Barra de botones")
        toolbar.setIconSize(QSize(16, 16))

        self.addToolBar(toolbar)
        button_action, dark_mode, button_action2 = self.setQAction(
            toolbar)
        """
        La parte del menú:
        """

        menu = self.menuBar()
        file_menu = menu.addMenu("File")
        file_menu.addAction(button_action)
        file_menu.addSeparator()
        file_submenu = file_menu.addMenu("Submenu")
        file_submenu.addAction(button_action2)


        window = menu.addMenu("Window")
        window.addAction(dark_mode)
        # self.addMenu()



    def setQAction(self, toolbar):
        filepath = Path(__file__).absolute().parent
        # print(filepath)
        icon_path = filepath.parent / "icons/icons/bug.png"
        dark_mode_icon = filepath.parent / "icons/propios/modo_nocturno.svg"
        # print("Existe icono", 
        #       icon_path.exists(), 
        #       type(icon_path))
        # toolbar.setToolButtonStyle(
        #     Qt.ToolButtonIconOnly)
        button_action = QAction(
            QIcon(str(icon_path)), "Acción", self)
        button_action.setToolTip(
            "Este es un botón de acción")
        button_action.setStatusTip(
            "Acción")

        button_action.triggered.connect(
            self.OnClickBotonAccion)

        button_action.setCheckable(True)

        # button 2
        icon_path2 = filepath.parent / "icons/icons/balance.png"

        button_action2 = QAction(
            QIcon(str(icon_path2)), "Your &button2", self)
        button_action2.triggered.connect(self.OnClickBotonAccion)
        button_action2.setCheckable(True)
        toolbar.addAction(button_action2)

        dark_mode = QAction(
            QIcon(str(dark_mode_icon)), "Modo Oscuro", self)
        dark_mode.setToolTip(
            "Cambiar a modo oscuro")
        dark_mode.setStatusTip(
            "Modo Oscuro")

        dark_mode.triggered.connect(
            self.OnClickBotonAccion)

        dark_mode.setCheckable(True)

        toolbar.addAction(button_action)
        toolbar.addSeparator()
        toolbar.addAction(dark_mode)

        return button_action, dark_mode, button_action2


    def OnClickBotonAccion(self, s):
        print("click", s)


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
