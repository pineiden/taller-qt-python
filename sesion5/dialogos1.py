import sys

from pathlib import Path

from PySide6.QtCore import Qt, QSize
from PySide6.QtGui import QAction, QIcon
from PySide6.QtWidgets import (
    QApplication,
    QLabel,
    QToolBar,
    QStatusBar,
    QPushButton,
    QMainWindow,
    QVBoxLayout,
    QDialogButtonBox,
    QMessageBox,
    QDialog)


class CustomDialog(QDialog):
    def __init__(self, parent=None):        
        super().__init__(parent)
        self.setWindowTitle("Dialogo!")
        # qt agrupacion de conjunto
        buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel

        self.buttonBox = QDialogButtonBox(buttons)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        
        self.layout = QVBoxLayout()
        message = QLabel("Algo ha pasado, esta todo ok?")
        self.layout.addWidget(message)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("App con menús")
        boton = QPushButton("Presioname para diálogo")
        boton.clicked.connect(self.button_clicked)
        self.setCentralWidget(boton)

    def ex_button_clicked(self, s):
        print("Click", s)
        dlg = CustomDialog(self)
        #dlg.setWindowTitle("?")
        if dlg.exec():
            print("Éxito!")
        else:
            print("Cancelando")
        
    def button_clicked(self, s):
        print("Click", s)
        dlg = QMessageBox(self)
        dlg.setWindowTitle("Tengo una pregunta!")
        dlg.setText("Esto es un dialogo simple")
        dlg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        dlg.setIcon(QMessageBox.Question)
        button = dlg.exec()
        if button == QMessageBox.Yes:
            print("Yes")
        else:
            print("NO")
        


if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
