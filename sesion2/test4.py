import sys

from PySide6.QtCore import QSize, Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton)

from rich.console import Console

import logging
# QWidget :: objeto general para widget 
# QPushButton :: objeto particular, hereda de QWidget
logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(message)s', 
    filename="ventana.log",
    level=logging.DEBUG
)

class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        # crea un boton con un texto
        self.button = QPushButton("Presioname :)")
        self.eventos_boton()
        #button.setFlat(True)
        self.set_window_size()
        # pone un botón en ventana
        self.setCentralWidget(self.button)
        self.console = Console()

        logging.info("Se inicia la ventana")

    def set_window_size(self):
        self.setFixedSize(QSize(700, 600))
        self.setMinimumSize(QSize(400, 200))
        self.setMaximumSize(QSize(1000, 800))
        logging.info("Se define el tamaño de venta")

    def eventos_boton(self):
        self.button.setCheckable(True)
        self.button.clicked.connect(
            self.boton_click)
        self.button.clicked.connect(
            self.boton_toggle)
        self.button.released.connect(
            self.boton_liberado)
        self.button_is_checked = self.button.isChecked()
        logging.info("Se configran eventos de botón")

    def boton_click(self):
        self.print("Se presiona el botón!")
        logging.info("Se hace click en botón")

    def boton_toggle(self, estado):
        self.print("Botón togleado", estado)
        logging.info(f"Se cambia estado de botón a {estado}")

    def boton_liberado(self):
        self.button_is_checked = self.button.isChecked()
        self.print("Botón liberado")
        logging.info(f"Se cambia libera el botón, estado {self.button_is_checked}")
        

    def print(self, *msg):
        self.console.print(
            *msg,
            style="red on blue")
        


if __name__ == "__main__":

    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
