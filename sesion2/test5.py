import sys

from PySide6.QtCore import QSize, Qt
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton)

from rich.console import Console
from random import choices

import logging
# QWidget :: objeto general para widget 
# QPushButton :: objeto particular, hereda de QWidget
logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(message)s', 
    filename="ventana.log",
    level=logging.DEBUG
)


titulos_ventana = [
    "Mi aplicación",
    "Mi ventanita",
    "Otra ventanina",
    "Ventana de accione de botón",
    "Algo pasa con esta ventana"
]

class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")
        self.windowTitleChanged.connect(
            self.cambio_titulo_ventana)
        # crea un boton con un texto
        self.button = QPushButton("Presioname :)")
        self.eventos_boton()
        #button.setFlat(True)
        self.set_window_size()
        # pone un botón en ventana
        self.setCentralWidget(self.button)
        self.console = Console()

        logging.info("Se inicia la ventana")

    def set_window_size(self):
        self.setFixedSize(QSize(700, 600))
        self.setMinimumSize(QSize(400, 200))
        self.setMaximumSize(QSize(1000, 800))
        logging.info("Se define el tamaño de venta")

    def eventos_boton(self):
        self.button.setCheckable(True)
        self.button.clicked.connect(
            self.boton_click)
        logging.info("Se configran eventos de botón")

    def boton_click(self):
        self.print("Se presiona el botón!")
        #self.button.setText("Ya me clickeaste!")
        #self.button.setEnabled(False)
        #self.setWindowTitle("Mi ventana de una sola acción!")
        nuevo_titulo = choices(titulos_ventana).pop()
        self.setWindowTitle(nuevo_titulo)
        logging.info("Se hace click en botón")

    def print(self, *msg):
        self.console.print(
            *msg,
            style="red on blue")
        
    def cambio_titulo_ventana(self, window_title):
        self.print(f"Se cambia título a ventana {window_title}")
        logging.info(f"Se cambia título a ventana {window_title}")
        if window_title == "Algo pasa con esta ventana":
            self.button.setDisabled(True)


if __name__ == "__main__":




    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
