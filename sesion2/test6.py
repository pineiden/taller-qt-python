import sys

from PySide6.QtCore import QSize, Qt
from PySide6.QtGui import QPixmap 
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QLabel,
    QLineEdit,
    QVBoxLayout,
    QWidget)

from rich.console import Console
from random import choices

import logging
# QWidget :: objeto general para widget 
# QPushButton :: objeto particular, hereda de QWidget
logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(message)s', 
    filename="ventana.log",
    level=logging.DEBUG
)


titulos_ventana = [
    "Mi aplicación",
    "Mi ventanita",
    "Otra ventanina",
    "Ventana de accione de botón",
    "Algo pasa con esta ventana"
]

class MainWindow(QMainWindow):
    console = None
    def __init__(self):
        # inicializa los elementos de la clase padre
        super().__init__()
        self.click_counter = 0
        # define el título de ventana
        self.setWindowTitle("Mi ventana Qt")

        # etiqueta. 
        self.label = QLabel("Acá va el texto") 
        self.label2 = QLabel("Imagen")
        self.label2.setPixmap(QPixmap("hugo.png"))
        font = self.label.font()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setAlignment(
            Qt.AlignLeft | Qt.AlignVCenter)
        self.text_input = QLineEdit()

        self.text_input.textChanged.connect(self.label.setText)

        layout = QVBoxLayout()
        layout.addWidget(self.text_input)
        layout.addWidget(self.label)
        layout.addWidget(self.label2)

        container = QWidget()
        container.setLayout(layout)

        # crea un boton con un texto
        #button.setFlat(True)
        self.set_window_size()
        # pone un botón en ventana
        self.setCentralWidget(container)
        self.console = Console()

        logging.info("Se inicia la ventana")

    def set_window_size(self):
        self.setFixedSize(QSize(700, 600))
        self.setMinimumSize(QSize(400, 200))
        self.setMaximumSize(QSize(1000, 800))
        logging.info("Se define el tamaño de venta")


    def print(self, *msg):
        self.console.print(
            *msg,
            style="red on blue")
        

if __name__ == "__main__":
    app = QApplication()
    window = MainWindow()
    window.show()
    app.exec()
